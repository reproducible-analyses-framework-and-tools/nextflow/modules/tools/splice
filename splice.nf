#!/usr/bin/env nextflow

include { manifest_to_raw_fqs } from '../preproc/preproc.nf'
include { raw_fqs_to_procd_fqs } from '../preproc/preproc.nf'

include { procd_fqs_to_alns } from '../alignment/alignment.nf'

include { trim_galore }        from '../trim_galore/trim_galore.nf'
include { trimmomatic }        from '../trimmomatic/trimmomatic.nf'

include { samtools_index } from '../samtools/samtools.nf'

include { spladder_build } from '../spladder/spladder.nf'
include { spladder_build_test } from '../spladder/spladder.nf'

include { neosplice } from '../neosplice/neosplice.nf'
include { neosplice_filter_and_summarize_variants } from '../neosplice/neosplice.nf'

include { altanalyze } from '../snaf/snaf.nf'
include { combine_altanalyze_counts_with_alleles } from '../snaf/snaf.nf'
include { snaf } from '../snaf/snaf.nf'

include { asneo } from '../asneo/asneo.nf'

workflow manifest_to_splice_variants {
// require:
//   params.splice$manifest_to_splice_variants$fq_trim_tool
//   params.splice$manifest_to_splice_variants$fq_trim_tool_parameters
//   params.splice$manifest_to_splice_variants$aln_tool
//   params.splice$manifest_to_splice_variants$aln_tool_parameters
//   params.splice$manifest_to_splice_variants$splice_var_caller
//   params.splice$manifest_to_splice_variants$splice_var_caller_parameters
//   params.splice$manifest_to_splice_variants$aln_ref
//   HLA_ALLELES
//   params.splice$manifest_to_splice_variants$gtf
//   params.splice$manifest_to_splice_variants$gff
//   params.splice$manifest_to_splice_variants$species
//   MANIFEST
  take:
    fq_trim_tool
    fq_trim_tool_parameters
    aln_tool
    aln_tool_parameters
    splice_var_caller
    splice_var_caller_parameters
    aln_ref
    hla_alleles
    gtf
    gff
    species
    manifest
  main:
    manifest_to_raw_fqs(
      manifest)
    raw_fqs_to_splice_variants(
      fq_trim_tool,
      fq_trim_tool_parameters,
      aln_tool,
      aln_tool_parameters,
      splice_var_caller,
      splice_var_caller_parameters,
      aln_ref,
      hla_alleles,
      gtf,
      gff,
      species,
      manifest_to_raw_fqs.out.fqs,
      manifest)
//  emit:
//    fqs = manifest_to_raw_fqs.out.fqs
//    procd_fqs = raw_fqs_to_splice_variants.out.procd_fqs
//    splice_vars = raw_fqs_to_splice_variants.out.splice_vars
}

workflow raw_fqs_to_splice_variants {
// require:
//   params.splice$raw_fqs_to_splice_variants$fq_trim_tool
//   params.splice$raw_fqs_to_splice_variants$fq_trim_tool_parameters
//   params.splice$raw_fqs_to_splice_variants$aln_tool
//   params.splice$raw_fqs_to_splice_variants$aln_tool_parameters
//   params.splice$raw_fqs_to_splice_variants$splice_var_caller
//   params.splice$raw_fqs_to_splice_variants$splice_var_caller_parameters
//   params.splice$raw_fqs_to_splice_variants$aln_ref
//   HLA_ALLELES
//   params.splice$raw_fqs_to_splice_variants$gtf
//   params.splice$raw_fqs_to_splice_variants$gff
//   params.splice$raw_fqs_to_splice_variants$species
//   FQS
//   MANIFEST
  take:
    fq_trim_tool
    fq_trim_tool_parameters
    aln_tool
    aln_tool_parameters
    splice_var_caller
    splice_var_caller_parameters
    aln_ref
    hla_alleles
    gtf
    gff
    species
    fqs
    manifest
  main:
    raw_fqs_to_procd_fqs(
      fqs,
      fq_trim_tool,
      fq_trim_tool_parameters)
    procd_fqs_to_splice_variants(
      aln_tool,
      aln_tool_parameters,
      splice_var_caller,
      splice_var_caller_parameters,
      aln_ref,
      hla_alleles,
      gtf,
      gff,
      species,
      raw_fqs_to_procd_fqs.out.procd_fqs,
      manifest)
//  emit:
//    procd_fqs = raw_fqs_to_procd_fqs.out.procd_fqs
//    splice_vars = procd_fqs_to_splice_variants.out.splice_vars
}

workflow procd_fqs_to_splice_variants {
// require:
//   params.splice$procd_fqs_to_splice_variants$aln_tool
//   params.splice$procd_fqs_to_splice_variants$aln_tool_parameters
//   params.splice$procd_fqs_to_splice_variants$splice_var_caller
//   params.splice$procd_fqs_to_splice_variants$splice_var_caller_parameters
//   params.splice$procd_fqs_to_splice_variants$aln_ref
//   HLA_ALLELES
//   params.splice$procd_fqs_to_splice_variants$gtf
//   params.splice$procd_fqs_to_splice_variants$gff
//   params.splice$procd_fqs_to_splice_variants$species
//   PROCD_FQS
//   MANIFEST
  take:
    aln_tool
    aln_tool_parameters
    splice_var_caller
    splice_var_caller_parameters
    aln_ref
    hla_alleles
    gtf
    gff
    species
    procd_fqs
    manifest
  main:
    procd_fqs_to_alns(
      procd_fqs,
      aln_tool,
      aln_tool_parameters,
      aln_ref,
      params.dummy_file)
    alns_to_splice_variants(
      procd_fqs_to_alns.out.alns,
      splice_var_caller,
      splice_var_caller_parameters,
      aln_ref,
      hla_alleles,
      gtf,
      gff,
      species,
      manifest)
}

workflow alns_to_splice_variants {
// require:
//   ALNS
//   params.splice$alns_to_splice_variants$splice_var_caller
//   params.splice$alns_to_splice_variants$splice_var_caller_parameters
//   params.splice$alns_to_splice_variants$splice_var_caller_ref
//   params.splice$alns_to_splice_variants$aln_ref
//   HLA_ALLELES
//   params.splice$alns_to_splice_variants$gtf
//   params.splice$alns_to_splice_variants$gff
//   params.splice$alns_to_splice_variants$species
//   MANIFEST
  take:
    alns
    splice_var_caller
    splice_var_caller_parameters
    splice_var_caller_ref
    aln_ref
    hlas
    gtf
    gff
    species
    manifest
  main:
    samtools_index(
      alns,
      '')
    manifest.filter{ it[5] =~ /TRUE/ }.set{ norms }
    manifest.filter{ it[5] =~ /FALSE/ }.set{ tumors }
    alns
      .join(samtools_index.out.bais, by: [0, 1])
      .set{ bams_bais }
    bams_bais
      .join(norms, by: [0, 1])
      .map{ [it[0], it[1], it[2], it[3], it[5]] }
      .set{ norm_bams_bais }
    bams_bais
      .join(tumors, by: [0, 1])
      .map{ [it[0], it[1], it[2], it[3], it[5]] }
      .set{ tumor_bams_bais }
    norm_bams_bais
      .combine(tumor_bams_bais)
      .map{ [it[5], it[7], it[1], it[3], it[4], it[6], it[8], it[9]] }
      .set{ norm_tumor_bams_bais }
    bams_bais
      .join(tumors, by: [0, 1])
      .map{ it[3] }
      .set{ all_tumor_bams }

    splice_var_caller_parameters = Eval.me(splice_var_caller_parameters)
    splice_var_caller_ref = Eval.me(splice_var_caller_ref)


    if( splice_var_caller =~ /spladder_build[,|$]/) {
      spladder_build(
        samtools_index.out.bams_and_bais,
        gtf,
        splice_var_caller_parameters)
    }
    if( splice_var_caller =~ /spladder_build_test/) {
      spladder_build_parameters = splice_var_caller_parameters['spladder_build'] ? splice_var_caller_parameters['spladder_build'] : ''
      spladder_test_parameters = splice_var_caller_parameters['spladder_test'] ? splice_var_caller_parameters['spladder_test'] : ''

      spladder_build_test(
        norm_tumor_bams_bais,
        gtf,
        spladder_build_parameters,
        spladder_test_parameters)
    }
    if( splice_var_caller =~ /neosplice/) {
      neosplice_augmented_splice_graph_build_parameters = splice_var_caller_parameters['neosplice_augmented_splice_graph_build'] ? splice_var_caller_parameters['neosplice_augmented_splice_graph_build'] : ''
      neosplice_get_max_kmer_length_parameters = splice_var_caller_parameters['neosplice_get_max_kmer_length'] ? splice_var_caller_parameters['neosplice_get_max_kmer_length'] : ''
      neosplice_convert_bams_to_fasta_parameters = splice_var_caller_parameters['neosplice_convert_bams_to_fasta'] ? splice_var_caller_parameters['neosplice_convert_bams_to_fasta'] : ''
      neosplice_get_splice_junctions_parameters = splice_var_caller_parameters['neosplice_get_splice_junctions'] ? splice_var_caller_parameters['neosplice_get_splice_junctions'] : ''
      neosplice_msbwt_is_parameters = splice_var_caller_parameters['neosplice_msbwt_is'] ? splice_var_caller_parameters['neosplice_msbwt_is'] : ''
      neosplice_convert_bwt_format_parameters = splice_var_caller_parameters['neosplice_convert_bwt_format'] ? splice_var_caller_parameters['neosplice_convert_bwt_format'] : ''
      neosplice_kmer_search_bwt_parameters = splice_var_caller_parameters['neosplice_kmer_search_bwt'] ? splice_var_caller_parameters['neosplice_kmer_search_bwt'] : ''
      neosplice_search_bam_parameters = splice_var_caller_parameters['neosplice_search_bam'] ? splice_var_caller_parameters['neosplice_search_bam'] : ''
      samtools_sort_parameters = splice_var_caller_parameters['neosplice_samtools_sort'] ? splice_var_caller_parameters['neosplice_samtools_sort'] : ''
      samtools_index_parameters = splice_var_caller_parameters['neosplice_samtools_index'] ? splice_var_caller_parameters['neosplice_samtools_index'] : ''
      neosplice_kmer_graph_inference_parameters = splice_var_caller_parameters['neosplice_kmer_graph_inference'] ? splice_var_caller_parameters['neosplice_kmer_graph_inference'] : ''
      neosplice_ref = splice_var_caller_ref['neosplice'] ? splice_var_caller_ref['neosplice'] : ''
      neosplice(
        manifest,
        samtools_index.out.bams_and_bais,
        aln_ref,
        gff,
        hlas,
        species,
        neosplice_augmented_splice_graph_build_parameters,
        neosplice_get_max_kmer_length_parameters,
        neosplice_convert_bams_to_fasta_parameters,
        neosplice_get_splice_junctions_parameters,
        neosplice_msbwt_is_parameters,
        neosplice_convert_bwt_format_parameters,
        neosplice_kmer_search_bwt_parameters,
        neosplice_search_bam_parameters,
        samtools_sort_parameters,
        samtools_index_parameters,
        neosplice_kmer_graph_inference_parameters)
      neosplice_filter_and_summarize_variants(
        neosplice.out.neoantigen_results,
        neosplice_ref)
    }
    if( splice_var_caller =~ /snaf/) {
      altanalyze_parameters = splice_var_caller_parameters['altanalyze'] ? splice_var_caller_parameters['altanalyze'] : ''
      snaf_parameters = splice_var_caller_parameters['snaf'] ? splice_var_caller_parameters['snaf'] : ''
      snaf_ref = splice_var_caller_ref['snaf'] ? splice_var_caller_ref['snaf'] : ''
      netmhcpan_ref = splice_var_caller_ref['netmhcpan'] ? splice_var_caller_ref['netmhcpan'] : ''
      // hla alleles here
      altanalyze(
        all_tumor_bams.toList(),
        altanalyze_parameters)
      combine_altanalyze_counts_with_alleles(
        altanalyze.out.counts,
        hlas.map{ it[3] }.toList())
      snaf(
        altanalyze.out.counts,
        combine_altanalyze_counts_with_alleles.out.snaf_alleles,
        snaf_ref,
        netmhcpan_ref)
    }
  emit:
    splice_c1_peptides = neosplice_filter_and_summarize_variants.out.splice_c1_peptides
    splice_summaries = neosplice_filter_and_summarize_variants.out.splice_summaries
}

workflow junctions_to_splice_variants {
// require:
//   JUNCTIONS
//   params.splice$junctions_to_splice_variants$splice_var_caller
//   params.splice$junctions_to_splice_variants$splice_var_caller_parameters
//   params.splice$junctions_to_splice_variants$splice_var_caller_ref
//   params.splice$junctions_to_splice_variants$aln_ref
//   HLA_ALLELES
  take:
    junctions
    splice_var_caller
    splice_var_caller_parameters
    splice_var_caller_ref
    aln_ref
    hlas
  main:
    splice_var_caller_parameters = Eval.me(splice_var_caller_parameters)
    splice_var_caller_ref = Eval.me(splice_var_caller_ref)

    if( splice_var_caller =~ /asneo/) {
      asneo_parameters = splice_var_caller_parameters['asneo'] ? splice_var_caller_parameters['asneo'] : ''

      junctions
        .join(hlas, by: [0, 1, 2])
        .set{ junctions_and_hlas }

      asneo(
        junctions_and_hlas,
        aln_ref,
        asneo_parameters)
    }
//  emit:
}
